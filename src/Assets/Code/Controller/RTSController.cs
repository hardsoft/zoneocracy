﻿using UnityEngine;
using System.Collections;
using Zoneocracy.World;

namespace Zoneocracy.Controller
{
	public class RTSController : MonoBehaviour
	{

		private static float HorizontalSpeed = 25f;
		private static float VerticalSpeed = 25f;
		private static float ZoomSpeed = 500f;
		//private static float RotateSpeed = 25f; // Doesn't seem to be required just yet.
		private static float ShiftModifier = 2.5f;
		private static float MouseEdgeDelta = 10f;

		private Texture marqueeTexture;
		private Vector2 marqueeStart;
		private Vector2 marqueeSize;
		private Rect marqueeRect;
		private Rect finalRect;

		void Start()
		{
			marqueeTexture = (Texture)Resources.Load("GUI/marqueeSelect");
		}

		void Update()
		{
			this.PerformSelections();
			this.CalculateTransformsAndTranslate();
		}

		private void OnGUI()
		{
			marqueeRect = new Rect(marqueeStart.x, marqueeStart.y, marqueeSize.x, marqueeSize.y);
			GUI.color = new Color(0, 100, 30, 0.3f);
			GUI.DrawTexture(marqueeRect, marqueeTexture);
		}

		private void PerformSelections()
		{
			// Set start point of marquee on mouse down
			if (Input.GetMouseButtonDown(0))
			{
				// Before doing anything, deselect all
				if (!this.MultipleSelectMultiplier)
				{
					Global.SelectionManager.DeselectAllSelectables();
				}

				// Get mouse position
				marqueeStart = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

				// If we can raycast a unit here, just select it.
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit))
				{
					hit.transform.gameObject.SendMessage("OnSelected", SendMessageOptions.DontRequireReceiver);
				}
			}

			// Select units and reset marquee when mouse button is let up
			if (Input.GetMouseButtonUp(0))
			{
				foreach (Selectable selectable in Global.SelectionManager.Selectables)
				{
					Vector3 screenPos = Camera.main.WorldToScreenPoint(selectable.transform.position);
					Vector2 screenPoint = new Vector2(screenPos.x, Screen.height - screenPos.y);

					if (finalRect.Contains(screenPoint))
					{
						selectable.SendMessage("OnSelected", SendMessageOptions.DontRequireReceiver);
					}
				}

				marqueeRect.width = 0;
				marqueeRect.height = 0;
				finalRect.width = 0;
				finalRect.height = 0;
				marqueeSize = Vector2.zero;
			}

			if (Input.GetMouseButton(0))
			{
				float _y = Screen.height - Input.mousePosition.y;
				marqueeSize = new Vector2(Input.mousePosition.x - marqueeStart.x, (marqueeStart.y - _y) * -1);

				// Rect.Contains only works for positively sized rectangles. Therefore, it is necessary
				// to draw a rectangle backward and check it for selectables in the case of
				// negative-width marquees.
				float left = marqueeRect.x;
				float top = marqueeRect.y;
				float width = marqueeRect.width;
				float height = marqueeRect.height;

				if (marqueeRect.width < 0)
				{
					left = marqueeRect.x - Mathf.Abs(marqueeRect.width);
					width = Mathf.Abs(marqueeRect.width);
				}
				if (marqueeRect.height < 0)
				{
					top = marqueeRect.y - Mathf.Abs(marqueeRect.height);
					height = Mathf.Abs(marqueeRect.height);
				}

				finalRect = new Rect(left, top, width, height);
			}
		}

		private void CalculateTransformsAndTranslate()
		{
			float currentRotation = transform.eulerAngles.y; // Get the current 0-360degree angle of the camera

			float h = 0; // horizontal
			float v = 0; // vertical
			float r = 0; // rotational
			float z = 0; // zoom

			// Get initial values based on input from axes
			this.CalculateInitialValuesFromAxes(out h, out v, out r, out z);

			// Account for camera rotation when translating
			float translateX = Mathf.Cos(currentRotation * Mathf.Deg2Rad) * h + Mathf.Sin(currentRotation * Mathf.Deg2Rad) * v;
			float translateZ = Mathf.Cos(currentRotation * Mathf.Deg2Rad) * v - Mathf.Sin(currentRotation * Mathf.Deg2Rad) * h;
			float translateY = -z;

			// Account for time between frames and apply speed modifiers
			translateX *= Time.deltaTime * RTSController.HorizontalSpeed;
			translateY *= Time.deltaTime * RTSController.ZoomSpeed;
			translateZ *= Time.deltaTime * RTSController.VerticalSpeed;

			// Check and account for Shift Key speed modifier
			if (this.UseShiftSpeedModifier)
			{
				translateX *= RTSController.ShiftModifier;
				translateY *= RTSController.ShiftModifier;
				translateZ *= RTSController.ShiftModifier;
			}

			// Translate and Rotate camera
			Vector3 vector = new Vector3(translateX, translateY, translateZ);
			this.PerformTransformAndRotate(vector, r);
		}

		private void CalculateInitialValuesFromAxes(out float h, out float v, out float r, out float z)
		{
			h = 0;
			v = 0;
			r = 0;
			z = 0;

			// Calculate rotation
			this.CalculateRotate(out r);

			// Calculate translation
			if (this.UseMouseZoomModifier)
			{
				this.CalculateMouseZoom(out z);
			}
			else if (this.UseMouseTranslateModifier)
			{
				this.CalculateMouseTranslate(out h, out v, out z);
			}
			else
			{
				this.CalculateDefault(out h, out v, out z);
			}
		}

		private void PerformTransformAndRotate(Vector3 vector, float r)
		{
			transform.Translate(vector, Space.World);
			// TODO: Rotate around a point slightly in front of the camera
			transform.Rotate(0, r, 0, Space.World);
		}

		#region Axis Calculations
		private void CalculateDefault(out float h, out float v, out float z)
		{
			if (this.UseScreenEdgeScrolling)
			{
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;
				h = 0;
				v = 0;

				if (mouseX <= 0 + RTSController.MouseEdgeDelta)
				{
					h = -1;
				}
				else if (mouseX >= Screen.width - RTSController.MouseEdgeDelta)
				{
					h = 1;
				}

				if (mouseY <= 0 + RTSController.MouseEdgeDelta)
				{
					v = -1;
				}
				else if (mouseY >= Screen.height - RTSController.MouseEdgeDelta)
				{
					v = 1;
				}
			}
			else
			{
				h = Input.GetAxis("Horizontal");
				v = Input.GetAxis("Vertical");
			}

			z = Input.GetAxis("Mouse ScrollWheel");
		}

		private void CalculateMouseTranslate(out float h, out float v, out float z)
		{
			h = Input.GetAxis("Mouse X") * -2;
			v = Input.GetAxis("Mouse Y") * -2;
			z = Input.GetAxis("Mouse ScrollWheel");
		}

		private void CalculateRotate(out float r)
		{
			if (this.UseMouseRotateModifier)
			{
				r = Input.GetAxis("Mouse X");
			}
			else
			{
				r = Input.GetAxis("Rotate");
			}
		}

		private void CalculateMouseZoom(out float z)
		{
			z = Input.GetAxis("Mouse Y") / 2;
		}
		#endregion

		#region Mouse Modifiers
		private bool UseMouseRotateModifier
		{
			get
			{
				return Input.GetMouseButton(2);
			}
		}

		private bool UseMouseTranslateModifier
		{
			get
			{
				return Input.GetMouseButton(1);
			}
		}

		private bool UseMouseZoomModifier
		{
			get
			{
				return Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
			}
		}

		private bool UseShiftSpeedModifier
		{
			get
			{
				return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
			}
		}

		private bool MultipleSelectMultiplier
		{
			get
			{
				return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
			}
		}

		private bool UseScreenEdgeScrolling
		{
			get
			{
				float mouseX = Input.mousePosition.x;
				float mouseY = Input.mousePosition.y;

				return
					(mouseX <= 0 + RTSController.MouseEdgeDelta) ||
					(mouseX >= Screen.width - RTSController.MouseEdgeDelta) ||
					(mouseY <= 0 + RTSController.MouseEdgeDelta) ||
					(mouseY >= Screen.height - RTSController.MouseEdgeDelta);
			}
		}
		#endregion
	}

}
