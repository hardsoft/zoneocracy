// Shader created with Shader Forge Beta 0.36 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.36;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:0,limd:0,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:True,tesm:0,blpr:0,bsrc:0,bdst:1,culm:0,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:32711,y:32670|custl-1266-OUT;n:type:ShaderForge.SFN_Tex2d,id:1153,x:33769,y:32114,ptlb:Texture_01,ptin:_Texture_01,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1243,x:33769,y:32305,ptlb:Texture_02,ptin:_Texture_02,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1244,x:33769,y:32605,ptlb:Texture_03,ptin:_Texture_03,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:1245,x:33500,y:32298|A-1153-RGB,B-1243-RGB,T-1247-G;n:type:ShaderForge.SFN_Lerp,id:1246,x:33500,y:32457|A-1245-OUT,B-1244-RGB,T-1247-B;n:type:ShaderForge.SFN_VertexColor,id:1247,x:33769,y:32463;n:type:ShaderForge.SFN_LightAttenuation,id:1254,x:33188,y:32717;n:type:ShaderForge.SFN_LightVector,id:1256,x:33933,y:32998;n:type:ShaderForge.SFN_Dot,id:1258,x:33609,y:32974,dt:0|A-1256-OUT,B-1402-OUT;n:type:ShaderForge.SFN_Add,id:1260,x:33140,y:33001|A-1270-OUT,B-1272-RGB;n:type:ShaderForge.SFN_LightColor,id:1264,x:33140,y:32868;n:type:ShaderForge.SFN_Multiply,id:1266,x:32956,y:32868|A-1254-OUT,B-1264-RGB,C-1260-OUT;n:type:ShaderForge.SFN_Color,id:1268,x:33772,y:32793,ptlb:Ambient Color,ptin:_AmbientColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:1270,x:33439,y:32873,cmnt:Diffuse Light|A-1246-OUT,B-1268-RGB,C-1258-OUT;n:type:ShaderForge.SFN_AmbientLight,id:1272,x:33439,y:33030;n:type:ShaderForge.SFN_HalfVector,id:1402,x:33845,y:33111;proporder:1153-1243-1244-1268;pass:END;sub:END;*/

Shader "Custom/NewShader" {
    Properties {
        _Texture_01 ("Texture_01", 2D) = "white" {}
        _Texture_02 ("Texture_02", 2D) = "white" {}
        _Texture_03 ("Texture_03", 2D) = "white" {}
        _AmbientColor ("Ambient Color", Color) = (1,1,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Texture_01; uniform float4 _Texture_01_ST;
            uniform sampler2D _Texture_02; uniform float4 _Texture_02_ST;
            uniform sampler2D _Texture_03; uniform float4 _Texture_03_ST;
            uniform float4 _AmbientColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(2,3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_1407 = i.uv0;
                float4 node_1247 = i.vertexColor;
                float3 finalColor = (attenuation*_LightColor0.rgb*((lerp(lerp(tex2D(_Texture_01,TRANSFORM_TEX(node_1407.rg, _Texture_01)).rgb,tex2D(_Texture_02,TRANSFORM_TEX(node_1407.rg, _Texture_02)).rgb,node_1247.g),tex2D(_Texture_03,TRANSFORM_TEX(node_1407.rg, _Texture_03)).rgb,node_1247.b)*_AmbientColor.rgb*dot(lightDirection,halfDirection))+UNITY_LIGHTMODEL_AMBIENT.rgb));
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Texture_01; uniform float4 _Texture_01_ST;
            uniform sampler2D _Texture_02; uniform float4 _Texture_02_ST;
            uniform sampler2D _Texture_03; uniform float4 _Texture_03_ST;
            uniform float4 _AmbientColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(2,3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_1408 = i.uv0;
                float4 node_1247 = i.vertexColor;
                float3 finalColor = (attenuation*_LightColor0.rgb*((lerp(lerp(tex2D(_Texture_01,TRANSFORM_TEX(node_1408.rg, _Texture_01)).rgb,tex2D(_Texture_02,TRANSFORM_TEX(node_1408.rg, _Texture_02)).rgb,node_1247.g),tex2D(_Texture_03,TRANSFORM_TEX(node_1408.rg, _Texture_03)).rgb,node_1247.b)*_AmbientColor.rgb*dot(lightDirection,halfDirection))+UNITY_LIGHTMODEL_AMBIENT.rgb));
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
