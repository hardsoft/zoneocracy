#warning Upgrade NOTE: unity_Scale shader variable was removed; replaced 'unity_Scale.w' with '1.0'

// Shader created with Shader Forge Beta 0.36 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.36;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:0,uamb:True,mssp:True,lmpd:False,lprd:True,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:0,x:32158,y:31935,cmnt:Boop|custl-64-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:37,x:32583,y:32004;n:type:ShaderForge.SFN_LightVector,id:42,x:33196,y:32642;n:type:ShaderForge.SFN_Dot,id:52,x:32923,y:32670,dt:1|A-42-OUT,B-62-OUT;n:type:ShaderForge.SFN_Add,id:55,x:32583,y:32288|A-84-OUT,B-187-RGB;n:type:ShaderForge.SFN_HalfVector,id:62,x:33196,y:32782;n:type:ShaderForge.SFN_LightColor,id:63,x:32583,y:32155;n:type:ShaderForge.SFN_Multiply,id:64,x:32402,y:32134|A-37-OUT,B-63-RGB,C-55-OUT;n:type:ShaderForge.SFN_Color,id:80,x:32983,y:32145,ptlb:Ambient Color,ptin:_AmbientColor,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:84,x:32760,y:32155,cmnt:Diffuse Light|A-1266-OUT,B-80-RGB,C-52-OUT,D-1368-OUT;n:type:ShaderForge.SFN_AmbientLight,id:187,x:32758,y:32307;n:type:ShaderForge.SFN_Lerp,id:1266,x:32983,y:31972|A-1417-OUT,B-1268-OUT,T-1272-RGB;n:type:ShaderForge.SFN_Multiply,id:1268,x:33549,y:32001|A-1270-RGB,B-1274-RGB;n:type:ShaderForge.SFN_Tex2d,id:1270,x:33549,y:32187,ptlb:Base Texture,ptin:_BaseTexture,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1272,x:33808,y:32139,ptlb:Color 1 Alpha,ptin:_Color1Alpha,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:1274,x:33811,y:31957,ptlb:Color 1,ptin:_Color1,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:1368,x:33153,y:32462|A-1389-OUT,B-1370-OUT,T-1374-RGB;n:type:ShaderForge.SFN_Multiply,id:1370,x:33549,y:32368|A-1270-RGB,B-1376-RGB;n:type:ShaderForge.SFN_Tex2d,id:1374,x:33808,y:32539,ptlb:Color 2 Alpha,ptin:_Color2Alpha,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:1376,x:33808,y:32345,ptlb:Color 2,ptin:_Color2,glob:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Relay,id:1389,x:33313,y:32331|IN-1270-RGB;n:type:ShaderForge.SFN_Relay,id:1417,x:33313,y:32261|IN-1270-RGB;proporder:1270-80-1272-1274-1374-1376;pass:END;sub:END;*/

Shader "Shader Forge/Examples/Custom Lighting" {
    Properties {
        _BaseTexture ("Base Texture", 2D) = "white" {}
        _AmbientColor ("Ambient Color", Color) = (1,1,1,1)
        _Color1Alpha ("Color 1 Alpha", 2D) = "white" {}
        _Color1 ("Color 1", Color) = (1,1,1,1)
        _Color2Alpha ("Color 2 Alpha", 2D) = "white" {}
        _Color2 ("Color 2", Color) = (1,1,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _AmbientColor;
            uniform sampler2D _BaseTexture; uniform float4 _BaseTexture_ST;
            uniform sampler2D _Color1Alpha; uniform float4 _Color1Alpha_ST;
            uniform float4 _Color1;
            uniform sampler2D _Color2Alpha; uniform float4 _Color2Alpha_ST;
            uniform float4 _Color2;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                LIGHTING_COORDS(2,3)
                float3 shLight : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.shLight = ShadeSH9(float4(mul(_Object2World, float4(v.normal,0)).xyz * 1.0,1)) * 0.5;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_1539 = i.uv0;
                float4 node_1270 = tex2D(_BaseTexture,TRANSFORM_TEX(node_1539.rg, _BaseTexture));
                float3 finalColor = (attenuation*_LightColor0.rgb*((lerp(node_1270.rgb,(node_1270.rgb*_Color1.rgb),tex2D(_Color1Alpha,TRANSFORM_TEX(node_1539.rg, _Color1Alpha)).rgb)*_AmbientColor.rgb*max(0,dot(lightDirection,halfDirection))*lerp(node_1270.rgb,(node_1270.rgb*_Color2.rgb),tex2D(_Color2Alpha,TRANSFORM_TEX(node_1539.rg, _Color2Alpha)).rgb))+UNITY_LIGHTMODEL_AMBIENT.rgb));
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers gles xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _AmbientColor;
            uniform sampler2D _BaseTexture; uniform float4 _BaseTexture_ST;
            uniform sampler2D _Color1Alpha; uniform float4 _Color1Alpha_ST;
            uniform float4 _Color1;
            uniform sampler2D _Color2Alpha; uniform float4 _Color2Alpha_ST;
            uniform float4 _Color2;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                LIGHTING_COORDS(2,3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_1540 = i.uv0;
                float4 node_1270 = tex2D(_BaseTexture,TRANSFORM_TEX(node_1540.rg, _BaseTexture));
                float3 finalColor = (attenuation*_LightColor0.rgb*((lerp(node_1270.rgb,(node_1270.rgb*_Color1.rgb),tex2D(_Color1Alpha,TRANSFORM_TEX(node_1540.rg, _Color1Alpha)).rgb)*_AmbientColor.rgb*max(0,dot(lightDirection,halfDirection))*lerp(node_1270.rgb,(node_1270.rgb*_Color2.rgb),tex2D(_Color2Alpha,TRANSFORM_TEX(node_1540.rg, _Color2Alpha)).rgb))+UNITY_LIGHTMODEL_AMBIENT.rgb));
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
