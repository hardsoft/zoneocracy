﻿namespace Zoneocracy.Actor
{
	using System;

	public static class ActorDefinitions
	{
		public const float ACTOR_ESSENTIALS_DEFAULT = 100f;
		public const float ACTOR_ATTRIBUTES_DEFAULT = 10f;
		public const float ACTOR_SKILLS_DEFAULT = 10f;
	}
}
