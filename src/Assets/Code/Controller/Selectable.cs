﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Zoneocracy.World;

namespace Zoneocracy.Controller
{
	public class Selectable : MonoBehaviour
	{

		private static string OutlineMaterialName = "Materials/Character_Outline";

		private bool isSelected;
		private List<Material> materialsList;

		void Start()
		{
			Global.SelectionManager.RegisterAsSelectable(this);

			materialsList = new List<Material>();
			materialsList.AddRange(gameObject.GetComponent<Renderer>().materials);
		}

		void Update()
		{

		}

		public bool IsSelected
		{
			get
			{
				return this.isSelected;
			}
			set
			{
				if (value == true && !this.isSelected)
				{
					this.isSelected = true;
					this.ActivateSelectionShader();
				}

				if (value == false && this.isSelected)
				{
					this.isSelected = false;
					this.DeactivateSelectionShader();
				}
			}
		}

		public void OnSelected()
		{
			this.IsSelected = true;
		}

		public void OnUnselected()
		{
			this.IsSelected = false;
		}

		// This would probably be better if we create key-value pairs and assign each
		// material a number. This would allow me to better standardize the setting of
		// materials and avoid null reference exceptions.
		private void ActivateSelectionShader()
		{
			if (!this.ListContainsShaderMaterial())
			{
				materialsList.Add(Resources.Load(Selectable.OutlineMaterialName, typeof(Material)) as Material);
			}

			this.RebuildMaterialsFromList();
		}

		private void DeactivateSelectionShader()
		{
			this.RemoveSelectedShaderFromMaterialsList();
			this.RebuildMaterialsFromList();
		}

		private void PrintMaterialsArrayContent(Material[] materials)
		{
			foreach (Material material in materials)
			{
				if (material != null)
				{
					Debug.Log(material.name);
				}
			}
		}

		private void RemoveSelectedShaderFromMaterialsList()
		{
			foreach (Material material in materialsList)
			{
				if (material.name == Selectable.OutlineMaterialName)
				{
					materialsList.Remove(material);
					return;
				}
			}
		}

		private bool ListContainsShaderMaterial()
		{
			foreach (Material material in materialsList)
			{
				if (material.name == Selectable.OutlineMaterialName)
				{
					return true;
				}
			}

			return false;
		}

		private void RebuildMaterialsFromList()
		{
			Material[] materials = new Material[materialsList.Count];

			int i = 0;
			foreach (Material material in materialsList)
			{
				materials[i] = material;
				i++;
			}

			gameObject.GetComponent<Renderer>().materials = materials;
		}

	}
}
