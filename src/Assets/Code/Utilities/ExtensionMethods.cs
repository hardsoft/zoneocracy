﻿namespace Zoneocracy.Utilities
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public static class ExtensionMethods
	{
		#region GameObject Extensions
		public static T GetComponentInParents<T>(this GameObject gameObject) 
			where T : Component
		{
			for (Transform transform = gameObject.transform; transform != null; transform = transform.parent)
			{
				T component = transform.GetComponent<T>();
				
				if (component != null)
				{
					return component;
				}
			}

			return null;
		}

		public static T[] GetComponentsInParents<T>(this GameObject gameObject)
			where T : Component
		{
			List<T> components = new List<T>();
			for (Transform transform = gameObject.transform; transform != null; transform = transform.parent)
			{
				T component = transform.GetComponent<T>();
				if (component != null)
				{
					components.Add(component);
				}
			}

			return components.ToArray();
		}

		public static void SetCollisionRecursive(this GameObject gameObject, bool enabled)
		{
			Collider[] colliders = gameObject.GetComponentsInChildren<Collider>();
			foreach (Collider collider in colliders)
			{
				collider.enabled = enabled;
			}
		}

		public static void SetVisibleRecursive(this GameObject gameObject, bool enabled)
		{
			Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
			foreach (Renderer renderer in renderers)
			{
				renderer.enabled = enabled;
			}
		}
		#endregion

		#region MonoBehavior Extensions
		#region TypeSafe Invoke
		public static void Invoke(this MonoBehaviour monoBehavior, Action action, float delay)
		{
			if (delay == 0f)
			{
				action();
			}
			else
			{
				monoBehavior.StartCoroutine(DelayedInvoke(action, delay));
			}
		}

		public static void Invoke<T>(this MonoBehaviour monoBehavior, Action<T> action, T arg, float delay)
		{
			if (delay == 0f)
			{
				action(arg);
			}
			else
			{
				monoBehavior.StartCoroutine(DelayedInvoke(action, arg, delay));
			}
		}

		public static void Invoke<T1, T2>(this MonoBehaviour monoBehavior, Action<T1, T2> action, T1 arg1, T2 arg2, float delay)
		{
			if (delay == 0f)
			{
				action(arg1, arg2);
			}
			else
			{
				monoBehavior.StartCoroutine(DelayedInvoke(action, arg1, arg2, delay));
			}
		}

		public static void Invoke<T1, T2, T3>(this MonoBehaviour monoBehavior, Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3, float delay)
		{
			if (delay == 0f)
			{
				action(arg1, arg2, arg3);
			}
			else
			{
				monoBehavior.StartCoroutine(DelayedInvoke(action, arg1, arg2, arg3, delay));
			}
		}

		public static void Invoke<T1, T2, T3, T4>(this MonoBehaviour monoBehavior, Action<T1, T2, T3, T4> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, float delay)
		{
			if (delay == 0f)
			{
				action(arg1, arg2, arg3, arg4);
			}
			else
			{
				monoBehavior.StartCoroutine(DelayedInvoke(action, arg1, arg2, arg3, arg4, delay));
			}
		}

		private static IEnumerator DelayedInvoke(Action action, float delay)
		{
			yield return new WaitForSeconds(delay);
		}

		private static IEnumerator DelayedInvoke<T>(Action<T> action, T arg, float delay)
		{
			yield return new WaitForSeconds(delay);
			action(arg);
		}

		private static IEnumerator DelayedInvoke<T1, T2>(Action<T1, T2> action, T1 arg1, T2 arg2, float delay)
		{
			yield return new WaitForSeconds(delay);
			action(arg1, arg2);
		}

		private static IEnumerator DelayedInvoke<T1, T2, T3>(Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3, float delay)
		{
			yield return new WaitForSeconds(delay);
			action(arg1, arg2, arg3);
		}

		private static IEnumerator DelayedInvoke<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, float delay)
		{
			yield return new WaitForSeconds(delay);
			action(arg1, arg2, arg3, arg4);
		}
		#endregion
		#endregion
	}
}
