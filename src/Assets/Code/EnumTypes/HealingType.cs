﻿namespace Zoneocracy.EnumTypes
{
	using System;

	public enum HealingType
	{
		HealingType_Organic,
		HealingType_Mechanical
	}
}
