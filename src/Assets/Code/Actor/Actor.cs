﻿namespace Zoneocracy.Actor
{
	using System;
	using Zoneocracy.EnumTypes;

	public class Actor : ZoneocracyBehavior
	{
		// Essentials
		private float health;
		private float healthMax;
		private float stamina;
		private float staminaMax;
		private string actorName;

		// Attributes
		private float toughness;
		private float intelligence;
		private float charisma;
		private float agility;

		// Skills
		private float marksmanship;
		private float melee;
		private float tactics;
		private float technology;

		// TODO: Faction Ownership

		/// <summary>
		/// Default constructor for an Actor.
		/// </summary>
		public Actor()
		{
			this.health = ActorDefinitions.ACTOR_ESSENTIALS_DEFAULT;
			this.healthMax = ActorDefinitions.ACTOR_ESSENTIALS_DEFAULT;
			this.stamina = ActorDefinitions.ACTOR_ESSENTIALS_DEFAULT;
			this.staminaMax = ActorDefinitions.ACTOR_ESSENTIALS_DEFAULT;

			this.toughness = ActorDefinitions.ACTOR_ATTRIBUTES_DEFAULT;
			this.intelligence = ActorDefinitions.ACTOR_ATTRIBUTES_DEFAULT;
			this.charisma = ActorDefinitions.ACTOR_ATTRIBUTES_DEFAULT;
			this.agility = ActorDefinitions.ACTOR_ATTRIBUTES_DEFAULT;

			this.marksmanship = ActorDefinitions.ACTOR_SKILLS_DEFAULT;
			this.melee = ActorDefinitions.ACTOR_SKILLS_DEFAULT;
			this.tactics = ActorDefinitions.ACTOR_SKILLS_DEFAULT;
			this.technology = ActorDefinitions.ACTOR_SKILLS_DEFAULT;
		}

		void Start()
		{

		}

		#region Essentials Properties
		public float Health
		{
			get
			{
				return this.health;
			}
			private set
			{
				this.health = value;
			}
		}

		public float HealthMax
		{
			get
			{
				return this.healthMax;
			}
			private set
			{
				this.healthMax = value;
			}
		}

		public float Stamina
		{
			get
			{
				return this.stamina;
			}
			private set
			{
				this.stamina = value;
			}
		}

		public float StaminaMax
		{
			get
			{
				return this.staminaMax;
			}
			private set
			{
				this.staminaMax = value;
			}
		}

		public string ActorName
		{
			get
			{
				return this.actorName;
			}
			private set
			{
				this.actorName = value;
			}
		}
		#endregion

		#region Attributes Properties
		public float Toughness
		{
			get
			{
				return this.toughness;
			}
			private set
			{
				this.toughness = value;
			}
		}

		public float Intelligence
		{
			get
			{
				return this.intelligence;
			}
			private set
			{
				this.intelligence = value;
			}
		}

		public float Charisma
		{
			get
			{
				return this.charisma;
			}
			private set
			{
				this.charisma = value;
			}
		}

		public float Agility
		{
			get
			{
				return this.agility;
			}
			private set
			{
				this.agility = value;
			}
		}
		#endregion

		#region Skills Properties
		public float Marksmanship
		{
			get
			{
				return this.marksmanship;
			}
			private set
			{
				this.marksmanship = value;
			}
		}

		public float Melee
		{
			get
			{
				return this.melee;
			}
			private set
			{
				this.melee = value;
			}
		}

		public float Tactics
		{
			get
			{
				return this.tactics;
			}
			private set
			{
				this.tactics = value;
			}
		}

		public float Technology
		{
			get
			{
				return this.technology;
			}
			private set
			{
				this.technology = value;
			}
		}
		#endregion

		public bool IsOrganic
		{
			get
			{
				return true;
			}
		}

		public void TakeDamage(float amount)
		{
			if (amount <= 0f)
			{
				return;
			}

			if (amount > this.Health)
			{
				this.Health = 0;
				// TODO: Some function that happens when the unit dies
				return;
			}

			this.Health -= amount;
		}

		public void HealDamage(float amount, HealingType healingType)
		{
			if ((this.IsOrganic && healingType != HealingType.HealingType_Organic) || 
			   (!this.IsOrganic && healingType != HealingType.HealingType_Mechanical))
			{
				return;
			}

			if (amount <= 0f)
			{
				return;
			}

			this.Health += amount;
			if (this.Health >= this.HealthMax)
			{
				this.Health = this.HealthMax;
			}
		}
	}
}
