﻿#region NamespaceInfo
// COLLAPSE ALL THE REGIONS TO SEE JUST CODE
// This is a namespace declaration. It's used by other classes to get access to anything
// public in this class. It should always be at the very beginning, and should follow the
// folder structure of the project, starting from the Code directory. The base namespace is "Zoneocracy"
//
// Example:
// This file is in Assets\Code\Example, so the namespace is Zoneocracy.Example
// A file in Assets\Code\UI\Menu should be under namespace Zoneocracy.UI.Menu
#endregion
namespace Zoneocracy.Example
{
	#region UsingInfo
	// This is where you declare you're using code from other namespaces
	//
	// Syntax: using [namespace];
	// Where namespace is the namespace that contains the classes you need access to
	// 
	// Please order them as follows:
	//
	// UnityEngine if the class is a MonoBehavior
	// Anything from the System namespace (in alphabetical order)
	// Anything from our namespace (in alphabetical order)
	#endregion
	using System.Collections;

	#region ClassInfo
	// This is the class declaration. You should have only one in a file, and it should match
	// the file name. For example, this is "ExampleClass.cs", so the class name should be ExampleClass
	//
	// Syntax: [access level] class [class name]
	// Access Level: Almost always public. Don't worry about other access levels for classes
	// Class Name: As above, the name of your class
	#endregion
	public class ExampleClass
	{
		#region MemberVariables
		// This is a variable definition. It's essentially the "data" that your class holds.
		// In this example, we have a number representing health.
		// These should pretty much always be private (we can give other classes access to it via functions)
		// The first word in a name for a variable like this should be lower case.
		//
		// examples:
		// private int stamina;
		// private bool isFlying;
		//
		// DO NOT INITIALIZE NON-STATIC VALUES HERE
		#endregion
		private int health;

		#region Fields
		// This is a field. It's how you control what changes can be made to your data. For example, you
		// don't want other classes to be able to just change your health to whatever they want. This is a
		// little overkill for this particular example, but it's a pattern we should strive to follow.
		// Notice this field is private. It can be public or private, depending on your needs.
		#endregion
		public int Health
		{
			// This is a getter. Return the value you want the other classes to be able to read.
			get
			{
				// Always reference non-static stuff from this class using "this.". It's safer that way.
				return this.health;
			}

			// This is a setter. Assume it takes an input named "value". Set the variable this field represents
			// equal to value.
			// Notice that the field is public but the setter is private. We don't want just anyone to be able
			// to change the health!
			private set
			{
				this.health = value;
			}
		}

		// This is another field that has no setter. It doesn't need one. We're just determining if we're dead!
		public bool IsDead
		{
			get
			{
				return this.Health <= 0;
			}
		}

		#region StaticFields
		// This is a static field. It means every instance of this class will contain this
		// value, and it cannot (and should not) be changed. To invoke this, from any other class
		// that imports the namespace above, you would call the class name. For example:
		// ExampleClass.CanFly; // returns true
		#endregion
		public static bool CanFly
		{
			get
			{
				return true;
			}
			// Notice there's no setter here. We don't need one.
		}

		#region DefaultConstructor
		// This is default constructor. It takes no parameters in initializes any member variables
		//
		// Syntax: public [class name]()
		//
		// There's no return type for constructors. C# knows that if you're naming a function exactly the same
		// as a class, it's a constructor. The parenthesis "()" are important, because it specifies that this
		// constructor takes no parameters. You would call this particular constructor like so:
		//
		// ExampleClass example = new ExampleClass();
		#endregion
		public ExampleClass()
		{
			// We initialize our member variables here
			this.health = 100;
		}

		#region Constructor
		// This is a constructor that takes a parameter. Notice inside the parenthesis, we have an integer
		// named starting health. If we call ExampleClass like we do above, but supply an integer number,
		// we will call this constructor instead, which will set health to the number that is passed in.
		//
		// ExampleClass example = new ExampleClass(150);
		// 
		// You can also pass other variables as parameters, like so:
		//
		// int someNumber = 125;
		// ExampleClass example = new ExampleClass(someNumber);
		#endregion
		public ExampleClass(int startingHealth)
		{
			this.health = startingHealth;
		}

		// This is a public function. It adjusts the health using the private field above.
		public void TakeDamage(int amount)
		{
			// This is a condition. If the amount of damage we're taking is negative (for some reason),
			// we don't want to HEAL. We have a different function for that below!
			if (amount < 0)
			{
				// Return ends the execution of the function immediately, without returning a value.
				return;
			}

			// We set our health to be health minus the amount passed to the function.
			this.Health = this.Health - amount;
		}

		// This does the opposite of TakeDamage
		public void HealDamage(int amount)
		{
			if (amount < 0)
			{
				return;
			}

			// This is a cool shortcut. It means the same as this.Health = this.Health = amount.
			// You can do the same with other operations, like -=.
			this.Health += amount;
		}

		// This is an example of a function that returns a value. We calculate how many times ExampleClass
		// can take a hit dealing the amount of damage passed to the function, and return it.
		// We use Integers in this exmaple, but in the future, it would be better to use float or double, so
		// we can have decimal places.
		public int HitsUntilDeath(int damage)
		{
			return this.Health / damage;
		}
	}
}