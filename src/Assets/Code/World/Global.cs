﻿namespace Zoneocracy.World
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class Global : ZoneocracyBehavior
	{
		private static Global instance;
		private static SelectionManager selectionManager;

		/// <summary>
		/// Returns the global instance. Public members of this class that are not static should
		/// be accessed through Global.Instance. You do not need to initialize this, it's a
		/// Singleton, which means it initializes itself on first use.
		/// </summary>
		public static Global Instance
		{
			get
			{
				return instance ?? (instance = new GameObject("Global").AddComponent<Global>());
			}
		}

		/// <summary>
		/// Returns the global selection manager singleton. At some point, we'll need to make this per-player.
		/// </summary>
		public static SelectionManager SelectionManager
		{
			get
			{
				return selectionManager ?? (selectionManager = new GameObject("Selection Manager").AddComponent<SelectionManager>());
			}
		}
	}
}
