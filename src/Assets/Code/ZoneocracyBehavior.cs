﻿
namespace Zoneocracy
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class ZoneocracyBehavior : MonoBehaviour
	{
		public T GetInterfaceComponent<T>()
			where T : class
		{
			return this.GetComponent(typeof(T)) as T;
		}

		public static List<T> FindObjectOfInterface<T>() 
			where T : class
		{
			MonoBehaviour[] monoBehaviors = FindObjectsOfType<MonoBehaviour>();
			List<T> list = new List<T>();

			foreach (MonoBehaviour behavior in monoBehaviors)
			{
				T component = behavior.GetComponent(typeof(T)) as T;

				if (component != null)
				{
					list.Add(component);
				}
			}

			return list;
		}
	}
}
