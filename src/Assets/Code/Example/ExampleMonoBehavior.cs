﻿// READ THIS
// This class is a similar implementation as ExampleClass, except it's a class
// Unity will understand and can put into the actual game world. The important
// differences are in the class declaration and the (lack of) constructors.
namespace Zoneocracy.Example
{
	// We need to import UnityEngine so that this class will inherit stuff from Unity
	using UnityEngine;
	using System.Collections;

	// Notice in our class declaration, we have " : MonoBehavior". This makes it so our class
	// inherits members from the MonoBehavior class. Doing this allows Unity to understand
	// that our class can be put into the game world.
	public class ExampleMonoBehavior : MonoBehaviour
	{
		private int health;

		// I've changed this field so that both get and set are public. Because we cannot have a constructor 
		// that takes a starting health parameter, we'll want to directly set the health immediately after
		// creating this object.
		public int Health
		{
			get
			{
				return this.health;
			}

			set
			{
				this.health = value;
			}
		}

		public bool IsDead
		{
			get
			{
				return this.Health <= 0;
			}
		}

		public static bool CanFly
		{
			get
			{
				return true;
			}
		}

		// This is where we would usually have constructors. We should not use constructors for
		// anything that is a MonoBehavior. Instead, we have this event called Start() which
		// is called when the object is created by Unity. Anything we would put in a constructor
		// goes here instead.
		//
		// NOTE, we cannot pass a parameter to this constructor. The previous example had one where
		// we could pass a value containing the starting health.
		//
		// Instead, we can set the health using public functions.
		void Start()
		{
			this.health = 100;
		}

		// This is another example of a useful event given to use by MonoBehavior. This function is called every frame.
		// We can do anything we want here. Some examples might include checking if we're dead, then playing a death
		// animation or something. Right now, it does nothing. But, use your imagination.
		void Update()
		{
			// Do stuff here.
		}

		public void TakeDamage(int amount)
		{
			if (amount < 0)
			{
				return;
			}

			this.Health = this.Health - amount;
		}

		public void HealDamage(int amount)
		{
			if (amount < 0)
			{
				return;
			}

			this.Health += amount;
		}

		public int HitsUntilDeath(int damage)
		{
			return this.Health / damage;
		}
	}
}
