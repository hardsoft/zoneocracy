﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Zoneocracy.Controller;

namespace Zoneocracy.World
{
	public class SelectionManager : MonoBehaviour
	{
		private List<Selectable> selectables;

		public SelectionManager()
		{
			this.selectables = new List<Selectable>();
		}

		public IEnumerable<Selectable> Selectables
		{
			get
			{
				return this.selectables;
			}
		}

		public void RegisterAsSelectable(Selectable selectable)
		{
			this.selectables.Add(selectable);
		}

		public void DeselectAllSelectables()
		{
			foreach (Selectable selectable in selectables)
			{
				selectable.OnUnselected();
			}
		}

	}

}
